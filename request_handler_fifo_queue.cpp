#include <iostream>
#include "request_handler_fifo_queue.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerFifoQueue )

//TODO fill in
RequestHandlerFifoQueue::RequestHandlerFifoQueue ( Json::Value &config ) : RequestHandler ( config ),
                                                                           perfModel ( Factory<PerfModel>::create (
                                                                                   config[ "perfModel" ] )) {

    startTimeOfCurrentJob = 0;
    remainingTimeOfJobs = 0;

}

RequestHandlerFifoQueue::~RequestHandlerFifoQueue ( ) {


}

void RequestHandlerFifoQueue::handleRequest ( Request *req, ListenerEnd<Request *> *completionCallback ) {

    //add to the queue
    req->setRemainingWork ( perfModel );
    if ( simulator::getSimTime ( ) + remainingTimeOfJobs + req->getRemainingWork ( ) < simulator::getSimTime ( )) {
        return;
    }
    requestsInFifoQueue.push ( req );


    remainingTimeOfJobs += req->getRemainingWork ( );// modify getSize

    //add completion event on time = current + remainingWork of req
    //std::cout<<"  *HANDLE_REQ*  req->getRemainingWork: "<<req->getRemainingWork ()<<"\n";

    //**************uint64_t completionTime = simulator::getSimTime ( ) + req->getRemainingWork ( );// modify getSize
    //change from the previous line to the next line made the fifo_queue work....
    uint64_t completionTime = simulator::getSimTime ( ) + remainingTimeOfJobs;


    //std::cout<<"*HANDLE_REQ*  "<<completionTime<<"\n";
    //addition of completion event to be made here
    addCompletionEvent ( completionTime, req );

    //--- if the queue is empty and job start, save this time
    //start the job if the queue-size is 1
    if ( requestsInFifoQueue.size ( ) == 1 ) {

        //startTimeOfCurrentJob = completionTime;
        startTimeOfCurrentJob = simulator::getSimTime ( );
        //notifyEnd ( requestsInFifoQueue.front ( ));



    }




    //have a map here, add the callback function to the map
    mappingOfCallbacksToRequests.insert ( std::make_pair ( req, completionCallback ));

//    //should I call the notifyEnd from here? Need to ask.....
//    notifyEnd ( req );


}

void RequestHandlerFifoQueue::notifyEnd ( Request *req ) {
    //remove the job from the queue
    remainingTimeOfJobs -= requestsInFifoQueue.front ( )->getRemainingWork ( );
    requestsInFifoQueue.pop ( );



    // call notifyListenerEnd
    // *****************  need to verify this
    std::map<Request *, ListenerEnd<Request *> *>::iterator it;

    it = mappingOfCallbacksToRequests.find ( req );

    if ( it != mappingOfCallbacksToRequests.end ( )) {
        //std::cout << "##############    " << typeid ( it->second ).name ( ) << " ################\n";
        //it->second->notifyEnd(req);

        //before calling notifyEnd remove the callback from the map

        ListenerEnd<Request *> *completionCallBackToBeRemoved = it->second;
        mappingOfCallbacksToRequests.erase ( it );
        //requestsInFifoQueue.pop ( );

        //**this should be the last thing that u do
        //call the notifyEnd of the callback function
        //it->second->notifyEnd ( req );
        completionCallBackToBeRemoved->notifyEnd ( req );
    }


    //create a completion event on currentTime + job size and add it to the queue--- and a job starts, save this time in the same var
    //std::cout<<"  *NOTIFY-END*    req->getRemainingWork: "<<req->getRemainingWork ()<<"\n";


//    uint64_t completionTime = simulator::getSimTime ( ) + req->getRemainingWork ( ); //modify getSize
//    std::cout<<" *NOTIFY_END * "<<completionTime<<"\n";
//    startTimeOfCurrentJob = completionTime;
//    addCompletionEvent ( completionTime, req );

    return;

}

unsigned int RequestHandlerFifoQueue::getQueueLength ( ) {

    return ( unsigned int ) requestsInFifoQueue.size ( );
}

uint64_t RequestHandlerFifoQueue::getRemainingWorkLeft ( ) {

    //get sum of all--- refer to the note taken in the notebook and also the photo taken.....
    //should i use the perf_model_service_rate method here???
    uint64_t doneTimeOfCurrentJob = simulator::getSimTime ( ) - startTimeOfCurrentJob;

    return remainingTimeOfJobs + doneTimeOfCurrentJob;

}
