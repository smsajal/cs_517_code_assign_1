#include "request_generator_dist.h"
#include "distribution.h"
#include "factory.h"

#include "time_helper.h"

REGISTER_CLASS( RequestGenerator, RequestGeneratorDist )

//TODO fill in

RequestGeneratorDist::RequestGeneratorDist ( Json::Value &config ) : RequestGenerator ( config ),
                                                                     requestGeneratorDistribution (
                                                                             Factory<Distribution>::create (
                                                                                     config[ "dist" ] )) {


}

RequestGeneratorDist::~RequestGeneratorDist ( ) {

}

Request *RequestGeneratorDist::nextRequest ( uint64_t arrivalTime ) {

    //what should I do here?
    double requestSizeInDouble = ( double ) requestGeneratorDistribution->nextRand ( );
    uint64_t requestSize;
    requestSize = ( uint64_t ) requestSizeInDouble;

    Request *generatedRequest = Request::create ( arrivalTime, requestSize );

    return generatedRequest;
}