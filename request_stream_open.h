#ifndef REQUEST_STREAM_OPEN_H
#define REQUEST_STREAM_OPEN_H

#include "json/json.h"
#include "request_stream.h"
#include "arrival_process.h"
#include "request_generator.h"


// Open-loop request sequence
class RequestStreamOpen : public RequestStream
{
private:
    //TODO fill in


    RequestGenerator* _requestGenerator;
    ArrivalProcess* _arrivalProcess;



public:
    RequestStreamOpen(Json::Value& config);
    virtual ~RequestStreamOpen();

    //TODO fill in
    virtual void init();
    virtual Request *next();


    //override notifyStart to call the addArrivalEvent after the call to the parent function....
    virtual void notifyStart ( Request *req );
};

#endif /* REQUEST_STREAM_OPEN_H */
