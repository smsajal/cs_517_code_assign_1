README
======

This document contains the documentation for this discrete event simulator assignment 1.

Assignment 1 components to implement:
-------------------------------------

The components for you to implement are marked with:

`//TODO fill in`

Here is a list of the files to implement:

* arrival_process_dist.cpp
* arrival_process_dist.h
* distribution_det.cpp
* distribution_det.h
* distribution_exp.cpp
* distribution_exp.h
* event.h
* perf_model_service_rate.cpp
* perf_model_service_rate.h
* request_generator_dist.cpp
* request_generator_dist.h
* request_handler.cpp
* request_handler_fifo_queue.cpp
* request_handler_fifo_queue.h
* request_stream.cpp
* request_stream_closed.cpp
* request_stream_closed.h
* request_stream_open.cpp
* request_stream_open.h
* simulator.cpp
* stats_latency.cpp
* stats_latency.h
* stats_throughput.cpp
* stats_throughput.h

The starter code has been designed to be modular and easily extensible. It may seem initially complex, but it will simplify the future extensions in later assignments. Additionally, the starter code is designed to reduce code repetition, so often, solutions may be functions with 0-2 lines of code. An empty function may very well be all that's necessary if there's nothing to do (e.g., in a destructor).

Suggested order of completing the assignment:
---------------------------------------------

1) distribution_det.cpp/h: There are only 3 functions to implement: the constructor, destructor, and nextRand. Each of these functions contain very few lines of code (e.g., 0-2 lines of code).
2) distribution_exp.cpp/h: There are only 3 functions to implement: the constructor, destructor, and nextRand. Each of these functions contain very few lines of code (e.g., 0-2 lines of code).
3) simulator.cpp: There are 5 functions to implement: schedule, removeEvent, run, run with stop time, and reset. You should figure out how to use a multiset for storing events. Each of these functions are 1-10 lines of code, with the two run functions looking very similar.
4)  a) request_stream.cpp/event.h: There are 2 functions to implement: addArrivalEvent and arrivalCallback. addArrivalEvent fills an event with the necessary data and calls the simulator schedule function. The arrivalCallback function calls notifyStart on the corresponding request stream. The arrivalCallback should be 1-2 lines, and if you're looking for some data to use in this function (e.g., which request stream to use, which request to use), that's precisely the necessary data that needs to be added to the event data. You should put that data in the ArrivalEventData struct in event.h.
    
    b) request_handler.cpp/event.h: This is just like the request_stream code, except for completion events rather than arrival events. The completionCallback should be calling the notifyEnd function on the corresponding request handler.
5) arrival_process_dist.cpp/h: There are only 3 functions to implement: the constructor, destructor, and nextArrival. Each of these functions contain very few lines of code (e.g., 0-2 lines of code). The arrival_process_dist basically represents the interarrival distribution, so you're essentially just using a distribution to generate interarrival times.
6) request_generator_dist.cpp/h: There are only 3 functions to implement: the constructor, destructor, and nextArrival. Each of these functions contain very few lines of code (e.g., 0-2 lines of code). The request_generator_dist basically represents the request size distribution, so you're essentially just using a distribution to generate request sizes. You create a request using Request::create(arrivalTime, requestSize).
7) request_stream_open.cpp/h: An open loop system is a combination of an arrival process and a request generator. You can create them using Factory<ArrivalProcess>::create(config[“arrivalProcessConfig”]) and Factory<RequestGenerator>::create(config["requestGeneratorConfig"]). You will need to call addArrivalEvent at the right point in time.
8) request_stream_closed.cpp/h: A closed loop system is a combination of a request generator and a think time distribution. You will need to call addArrivalEvent at the right point in time.
9) perf_model_service_rate.cpp/h: There are only 3 functions to implement: the constructor, destructor, and estimateWork. Each of these functions contain very few lines of code (e.g., 0-2 lines of code). The performance model represents the speed of a server (here, in bytes/sec), and estimateWork converts the request size to the amount of time to process a request.
10) request_handler_fifo_queue.cpp/h: This represents a First-In First-Out (FIFO) queue where you simulate the behavior in a queue. This is probably the hardest part of the assignment, with each function taking about 0-10 lines of code. When a request is complete, make sure you call notifyListenersEnd to tell any component that is interested in request completions at that queue (e.g., for statistics gathering). Also when a request is complete, as the absolute last step, you should call notifyEnd on the completionCallback that was originally passed in through handleRequest. This is used to signal that the request is done to whomever sent the request. Note that you should keep track of this completionCallback, since you get the pointer in handleRequest, but you need to use it in another function when the request is complete.
10) stats_throughput.cpp/h: This is just counting and keeping track of requests. The important part is to remember to call checkOutputStats at the beginning of notifyEnd, and if it returns true, then you should call resetStats.
11) stats_latency.cpp/h: This is similar to stats_throughput, except you track latency sums as well as request counts.

Hints:
------

* To call a parent constructor with a parameter from a child constructor, you would put it first in the initializer list as ParentClass(param).
* To access the JSON config data, you would use something like config["key_name"].asDouble(). So for example, config["val"].asDouble() will provide the deterministic distribution value for DistributionDet.
* Take caution when determining when to erase an event from the event queue in simulator.cpp.
* RequestStream::addArrivalEvent should be taking advantage of polymorphism.
* Most of the objects in this simulator can be created with the factory method. For example, to create a Distribution, you can use Factory<Distribution>::create(config["dist"]). This will appropriately track it and automatically free it at the end of the program. It will also use the "type" in the JSON config to create the correct distribution type automatically.
* Look at time_helper.h for functions to convert between seconds (double) and nanoseconds (uint64_t).
* To create a request, you use Request::create(arrivalTime, requestSize). To destroy a request, you use req->destroy(). Requests should already be destroyed in the base RequestStream class's notifyEnd function.
* When overriding functions, consider whether you should be calling the parent class's function, and if so, at what point in the derived class's function.

Summary of classes/files:
-------------------------

ArrivalProcess - base class representing an open-loop sequence of arrivals over time

ArrivalProcessDist - arrival process with i.i.d. interarrival times following a given distribution

Distribution - base class representing a distribution

DistributionDet - deterministic distribution

DistributionExp - exponential distribution

Event - simulator event class

Factory - factory design pattern for constructing objects

Listener - base class for supporting listening capability

main - contains the main function

Notifier - base class for supporting notification capability

ObjectPool - optimizes creation/destruction of frequently used objects (e.g., Request)

PerfModel - represents the performance model of a queue

PerfModelServiceRate - represents a queue that has a constant serviceRate (bytes/sec)

random_helper - contains a global c++ random number generator and helper functions for seeding the generator

Request - class representing a request

RequestGenerator - base class for generating requests

RequestGeneratorDist - request generator for i.i.d. request sizes following a given distribution

RequestHandler - base class for an object that handles requests (e.g., queue, server, load balancer, etc)

RequestHandlerFifoQueue - simple first-in-first-out queue

RequestStream - base class for a sequence of requests

RequestStreamClosed - class representing a closed-loop request sequence

RequestStreamOpen - class representing an open-loop request sequence where the arrival process and request generation is independent of each other

run - contains the code to setup/teardown a simulation based on a JSON configuration (see below)

simulator - core discrete event simulator global functions

Stats - base class for statistics tracking

StatsLatency - measure mean latency of a RequestHandler

StatsThroughput - measure throughput of a RequestHandler

Simulator input configuration:
------------------------------

The simulator is configured via a JSON configuration format. JSON is an easy-to-use format for representing dictionaries (i.e., key-value pairs) and lists. Manipulating JSON is handled via the open-source JsonCpp library, included in the json directory. Two example config files are provided: tests/example_open.json and tests/example_closed.json. The format of the config file is as follows:

### Root level config:

* "seed": (optional) unsigned int - random number generator seed
* "stopTime": (optional) unsigned int - time to stop simulation (nanoseconds)
* "stats": (optional) list of stats configs - statistics to gather
* "requestHandlers": (optional) list of request handler configs - queues, servers, load balancers, etc in the simulation
* "requestStreams": (required) list of request stream configs - workloads in the simulation
* "distributions": (optional) list of distribution configs - distributions in the simulation
* "perfModels": (optional) list of performance model configs - performance models in the simulation

### Stats config:

* "name": (required) string - name to identify the stats collector
* "type": (required) string - stats class name (e.g., StatsLatency)
* "outputJson": (optional) bool - if true, stats will be included in output file
* "outputPrint": (optional) bool - if true, stats will be print to stdout while simulator runs
* "outputIntervalNs": (optional) unsigned int - time interval for outputting stats (nanoseconds)
* "targets": (required) list of strings - names of objects to collect stats for

### Request handler config:

* "name": (required) string - name to identify the request handler
* "type": (required) string - request handler class name (e.g., RequestHandlerFifoQueue)
* "perfModel": (required) performance model config or string - performance model config or name of performance model

### Request stream config:

* "name": (required) string - name to identify the request stream
* "type": (required) string - request stream class name (e.g., RequestStreamOpen)
* "requestHandler": (required) request handler config or string - request handler config or name of request handler to send stream of requests to
* "requestGeneratorConfig": (required for RequestStreamOpen and RequestStreamClosed) request generator config or string - request generator config or name of request generator
* "arrivalProcessConfig": (required for RequestStreamOpen) arrival process config or string - arrival process config or name of arrival process
* "thinkTime": (required for RequestStreamClosed) distribution config or string - think time distribution or name of think time distribution
* "MPL": (required for RequestStreamClosed) unsigned int - number of concurrent requests

### Request generator config:

* "name": (required) string - name to identify the request generator
* "type": (required) string - request generator class name (e.g., RequestGeneratorDist)
* "dist": (required for RequestGeneratorDist) distribution config or string - request size distribution (bytes) or name of request size distribution

### Arrival process config:

* "name": (required) string - name to identify the arrival process
* "type": (required) string - arrival process class name (e.g., ArrivalProcessDist)
* "dist": (required for ArrivalProcessDist) distribution config or string - interarrival distribution (seconds) or name of interarrival distribution

### Distribution config:

* "name": (required) string - name to identify the distribution
* "type": (required) string - distribution class name (e.g., DistributionExp)
* "rate": (required for DistributionExp) double - exponential distribution rate
* "val": (required for DistributionDet) double - deterministic distribution value

### Performance model config:
* "name": (required) string - name to identify the performance model
* "type": (required) string - performance model class name (e.g., PerfModelServiceRate)
* "serviceRate": (required for PerfModelServiceRate) double - service rate in bytes/sec

Your implementation should follow the above config format and support all of the options. The "name" and "type" keys are automatically handled as part of factory.h. Some of the other keys are handled in the starter code. You are responsible for supporting the rest of the options that aren't setup in the starter files.

When creating config files, note that all object names should be unique. Managing the creation/destruction of all these objects are handled via the factory design pattern (see factory.h), and is already setup in the starter code. Adding an additional class type only requires including factory.h and using the REGISTER_CLASS macro.

To build the code:
------------------

Run:

`make`

The makefile supports incremental builds and automatically tracks header dependencies. To add another source file, simply add OBJS += source_file.o to the Makefile.

To run the simulator:
---------------------

Run:

`./simulator tests/example_open.json`

The parameter contains the path of the config file (see above). The output file will be written to pathname_res.json. For example, tests/example_open.json_res.json. The output will be in JSON format, and will contain the entire input file, along with the list of stats indexed by name in "results".

Test code:
----------

Test code is located in the tests directory and is written in the Catch framework (see tests/catch/catch.hpp). To build the test code, change to the tests directory and run:

`make`

Then the tests can be run via:

`./simulatorTest`

The grading script can be run via:

`./grade.py`

Note that while some test cases are provided, it is not exhaustive, and you should perform your own testing and supplement the tests as you see fit.

Handin instructions:
--------------------

Run:

`make handin`

The Makefile is set to automatically clean when generating the handin, but please remove any large files unaffected by make clean. Upload the resulting handin.tar.gz to canvas.
