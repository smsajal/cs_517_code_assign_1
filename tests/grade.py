#!/usr/bin/env python

import os

if __name__ == '__main__':
    tests = [("DistributionDet", 5),
             ("DistributionExp", 5),
             ("Simulator", 10),
             ("ArrivalProcessDist", 5),
             ("RequestGeneratorDist", 5),
             ("RequestStreamOpen", 10),
             ("RequestStreamClosed", 10),
             ("StatsThroughput", 10),
             ("StatsLatency", 10),
             ("RequestHandlerFifoQueue", 15),
             ("overall", 15),
    ]
    points = 0
    for testName, testValue in tests:
        status = os.system("./simulatorTest [%s]" %(testName))
        if status == 0:
            print "%s [%dpts]: passed" %(testName, testValue)
            points += testValue
        else:
            print "%s [%dpts]: failed" %(testName, testValue)
    print "Total score: %d" %(points)
