#include <iostream>
#include <cassert>
#include <set>
#include "simulator.h"

using std::cout;

namespace simulator {
    // Event queue in timestamp order
    std::multiset <Event> queue;
    // Global simulator time (nanoseconds)
    uint64_t simTime = 0;

    // Add an event to the event queue
    EventReference schedule ( const Event &e ) {
        //std::cout<<"Inside simulator::schedule...\n";
        //cout<<"e.timestamp: "<<e.timestamp<<"   simTime: "<<getSimTime ()<<"\n";
        assert ( e.timestamp >= getSimTime ( )); // ensure we don't go back in time


        //TODO fill in
        return queue.insert ( e );

    }


    // Remove an event from the event queue
    void removeEvent ( EventReference eventRef ) {
        //TODO fill in

        queue.erase ( eventRef );

        return;
    }


    // Run simulation until no more events
    void run ( ) {

        //TODO fill in

        //for ( EventReference eventReference = queue.begin ( ); eventReference != queue.end ( ); ++eventReference ) {
        while ( !queue.empty ( )) {

            // use a while loop here....
            EventReference eventReference = queue.begin ( );



            simTime = eventReference->timestamp;
            //std::cout << eventReference->timestamp << " from simulator::run()...\n";
            //invoke call to the callback function---- have to verify this.......
            eventReference->callback ( *eventReference );

            //removal of the event from event queue
            removeEvent ( eventReference );

        }
        return;


    }


    // Run simulation until stopTime (nanoseconds)
    void run ( uint64_t stopTime ) {
        //TODO fill in
        //for ( EventReference eventReference = queue.begin ( ); eventReference != queue.end ( ); ++eventReference ) {
        //while loop here
        while ( !queue.empty ( )) {
            EventReference eventReference = queue.begin ( );

            //std::cout << eventReference->timestamp << " from simulator::run( stopTime )...\n";
            // check if time is up....
            if ( eventReference->timestamp > stopTime ) {
                //cout << "timeout for the simulation...\n";
                break;
            }


            simTime = eventReference->timestamp;
            //std::cout << eventReference->timestamp << " from simulator::run( stopTime )...\n";
            //invoke call to the callback function---- have to verify this.......
            eventReference->callback ( *eventReference );

            //removal of the event from event queue
            removeEvent ( eventReference );


        }

        simTime = stopTime;

        return;

    }


    // Reset simulation
    void reset ( ) {
        //TODO fill in
        //std::cout<<"inside simulator::reset...\n";
        //EventReference beginningOfQueue = queue.begin ( );

        queue.erase ( queue.begin ( ), queue.end ( ));

        simTime = 0;

        return;
    }
}
