#include "request_stream.h"
#include "request.h"
#include "event.h"

#include "simulator.h"
#include <iostream>

// Add an arrival event to the event queue for the next request in reqStream
void RequestStream::addArrivalEvent ( ) {
    //TODO fill in
    struct Event arrivalEvent;
    //std::cout<<"inside RequestStream::addArrivalEvent()...\n";
    Request *request = this->next ( );

    if ( request ) {

        //std::cout<<"inside RequestStream::addArrivalEvent()=> request is found...\n";

        //set the timestamp
        //get the arrival time for timestamp
        arrivalEvent.timestamp = request->getArrivalTime ( );


        //set callback to arrivalCallback
        arrivalEvent.callback = &arrivalCallback;


        //set this request-stream
        arrivalEvent.data.arrivalEvent.arrivalRequest = request;
        arrivalEvent.data.arrivalEvent.arrivalRequestStream = this;

        Event &eventReference = arrivalEvent;
        //simulator::EventReference arrivalEventReference = simulator::schedule ( eventReference );
        simulator::schedule ( eventReference );

    }


    return;
}

// Arrival callback function
void arrivalCallback ( const Event &e ) {
    //TODO fill in
    //std::cout<<"inside arrivalCallback...\n";
    //call notify start....
    e.data.arrivalEvent.arrivalRequestStream->notifyStart ( e.data.arrivalEvent.arrivalRequest );

    return;
}
